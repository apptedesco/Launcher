package paris.tedesco.paul.Launcher;

import static fr.theshark34.swinger.Swinger.drawFullsizedImage;
import static fr.theshark34.swinger.Swinger.getResource;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import fr.litarvan.openauth.AuthenticationException;
import fr.theshark34.openlauncherlib.launcher.util.UsernameSaver;
import fr.theshark34.swinger.Swinger;
import fr.theshark34.swinger.colored.SColoredBar;
import fr.theshark34.swinger.event.SwingerEvent;
import fr.theshark34.swinger.event.SwingerEventListener;
import fr.theshark34.swinger.textured.STexturedButton;


@SuppressWarnings("serial")
public class LauncherPanel extends JPanel implements SwingerEventListener{
	
	private String rememberUser;
	
	private Image background = getResource("background.png");
	
	private UsernameSaver saver = new UsernameSaver(Launcher.LW_INFOS);
	
	private UsernameSaver crackSaver = new UsernameSaver(Launcher.LW_INFOS);
	
	private JTextField usernameField = new JTextField(saver.getUsername(""));
	
	private JTextField carckUsernameField = new JTextField(saver.getUsername(""));
	
	private JTextField passwordField = new JPasswordField();
	
	
	private STexturedButton playButton = new STexturedButton(getResource("play.png"));
	private STexturedButton crackButton = new STexturedButton(getResource("play.png"));
	private STexturedButton closeButton = new STexturedButton(getResource("close.png"));
	private STexturedButton hideButton = new STexturedButton(getResource("negative-sign.png"));
	private JCheckBox remember = new JCheckBox("Se Souvenir");
	
	private SColoredBar progressBar = new SColoredBar(Swinger.getTransparentWhite(50), Color.GREEN);
	private JLabel infoLabel = new JLabel("Click sur Jouer !", SwingConstants.CENTER);
	
	private void setFieldsEnabled(boolean enabled)
	{
		usernameField.setEnabled(enabled);
		passwordField.setEnabled(enabled);
		playButton.setEnabled(enabled);
		
		carckUsernameField.setEnabled(enabled);
		crackButton.setEnabled(enabled);
	}
	
	public SColoredBar getProgressBar()
	{
		return progressBar;
	}
	
	public void setInfoText(String text)
	{
		infoLabel.setText(text);
	}


	public LauncherPanel()
	{
		this.setLayout(null);
		usernameField.setFont(usernameField.getFont().deriveFont(20F));
		usernameField.setOpaque(false);
		usernameField.setBorder(null);
		usernameField.setBounds(145, 135, 266, 30);
		this.add(usernameField);
		
		passwordField.setFont(usernameField.getFont().deriveFont(20F));
		passwordField.setOpaque(false);
		passwordField.setBorder(null);
		passwordField.setBounds(145, 205, 266, 30);
		this.add(passwordField);
		
		playButton.setBounds(145, 300);
		playButton.addEventListener(this);
		this.add(playButton);
		
		carckUsernameField.setFont(usernameField.getFont().deriveFont(20F));
		carckUsernameField.setOpaque(false);
		carckUsernameField.setBorder(null);
		carckUsernameField.setBounds(475, 135, 266, 30);
		this.add(carckUsernameField);
		
		crackButton.setBounds(475, 190);
		crackButton.addEventListener(this);
		this.add(crackButton);
		
		closeButton.setBounds(954,15);
		closeButton.addEventListener(this);
		this.add(closeButton);
		
		hideButton.setBounds(900,15);
		hideButton.addEventListener(this);
		this.add(hideButton);
		
		infoLabel.setBounds(30, 550, 870,25);
		infoLabel.setFont(usernameField.getFont());
		infoLabel.setForeground(Color.WHITE);
		this.add(infoLabel);
		
		remember.setBounds(145, 260, 250, 25);
		remember.setOpaque(false);
		this.add(remember);
		
		
		
		
		progressBar.setBounds(30, 572, 870,20);
		this.add(progressBar);

	}
	
	@Override
	public void onEvent(SwingerEvent e)
	{
		if(e.getSource() == playButton)
		{
			setFieldsEnabled(false);
			if(usernameField.getText().replaceAll(" ", "").length() == 0 || passwordField.getText().length() == 0)
			{
				JOptionPane.showMessageDialog(this, "Erreur, veuillez entrer un pseudo et un mot de passe valides." , "Erreur", JOptionPane.ERROR_MESSAGE);
				setFieldsEnabled(true);
				return;
			}
			Thread t = new Thread()
					{
						@Override
						public void run() {
							try {
								if(usernameField.getText().equals("steodec"))
									Launcher.auth("steodec.tedo@gmail.com", passwordField.getText());
								else if (usernameField.getText().equals("admin"))
									Launcher.auth("lionello@free.fr", passwordField.getText());
								else
									Launcher.auth(usernameField.getText(), passwordField.getText());
							}
							catch (AuthenticationException e)
							{
								JOptionPane.showMessageDialog(LauncherPanel.this, "Erreur, Impossible de se connecter :" + e.getErrorModel().getErrorMessage() , "Erreur", JOptionPane.ERROR_MESSAGE);
								setFieldsEnabled(true);
								return;
							}
							try {
							Launcher.update();
							}
							catch (Exception e)
							{
								Launcher.interruptThread();
								JOptionPane.showMessageDialog(LauncherPanel.this, "Erreur, impossible de mettre le jeux � jour :" + e.getMessage() , "Erreur", JOptionPane.ERROR_MESSAGE);
								setFieldsEnabled(true);
								return;
							}
							try {
							Launcher.launch();
							}
							catch (Exception e)
							{
								JOptionPane.showMessageDialog(LauncherPanel.this, "Erreur, impossible de Lancer le jeux :" + e.getMessage() , "Erreur", JOptionPane.ERROR_MESSAGE);
								setFieldsEnabled(true);
							}
							
						}
					};
					t.start();
		}
		else if (e.getSource() == crackButton) {
			setFieldsEnabled(false);
			if(carckUsernameField.getText().replaceAll(" ", "").length() == 0)
			{
				JOptionPane.showMessageDialog(this, "Erreur, veuillez entrer un pseudo." , "Erreur", JOptionPane.ERROR_MESSAGE);
				setFieldsEnabled(true);
				return;
			}
			Thread t = new Thread()
					{
						@Override
						public void run() {
							try {
									Launcher.crack(usernameField.getText());
							}
							catch (AuthenticationException e)
							{
								JOptionPane.showMessageDialog(LauncherPanel.this, "Erreur, Impossible de se connecter :" + e.getErrorModel().getErrorMessage() , "Erreur", JOptionPane.ERROR_MESSAGE);
								setFieldsEnabled(true);
								return;
							}
							try {
							Launcher.update();
							}
							catch (Exception e)
							{
								Launcher.interruptThread();
								JOptionPane.showMessageDialog(LauncherPanel.this, "Erreur, impossible de mettre le jeux � jour :" + e.getMessage() , "Erreur", JOptionPane.ERROR_MESSAGE);
								setFieldsEnabled(true);
								return;
							}
							try {
							Launcher.launch();
							}
							catch (Exception e)
							{
								JOptionPane.showMessageDialog(LauncherPanel.this, "Erreur, impossible de Lancer le jeux :" + e.getMessage() , "Erreur", JOptionPane.ERROR_MESSAGE);
								setFieldsEnabled(true);
							}
							
						}
					};
					t.start();
		}
		else if(e.getSource() == closeButton)
			System.exit(0);
		else if(e.getSource() == hideButton)
			LauncherFrame.getInstance().setState(JFrame.ICONIFIED);
			
	}
	
	@Override
	  public void paintComponent(Graphics graphics) { super.paintComponent(graphics);
	    drawFullsizedImage(graphics, this, background);
	  }

}
